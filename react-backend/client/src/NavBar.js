import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

class NavBar extends Component {

    render() {
        return( 
            <AppBar position="static">
            <Toolbar>
              <IconButton edge="start" color="inherit" aria-label="menu" style={{marginRight: '90%'}}>
                Title
              </IconButton>
              <Typography variant="h6" >
                News
              </Typography>
              <Button color="inherit">Login</Button>
            </Toolbar>
          </AppBar>
        )
    }

};

export default NavBar;