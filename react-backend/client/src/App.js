import React, { Component } from 'react';

import Paper from '@material-ui/core/Paper';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import 'typeface-roboto';
import beach from './beach.jpg';

import Jumbotron from 'react-bootstrap/Jumbotron'

import NavBar from './NavBar'; 

class App extends Component {
  state = {users: []}

  componentDidMount() {
    fetch('/users')
      .then(res => res.json())
      .then(users => this.setState({ users }));
  }

  render() {
    return (
      <div>
        <NavBar/>
        <Paper style={{marginLeft: '10%', marginRight: '10%'}} elevation={1}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Jumbotron>
                <img src={beach} alt="" style={{maxWidth: '100%', maxHeight: '100%'}}/>
            </Jumbotron>
          </Grid>
              <Grid item xs={6}>
                  <h3>Words</h3>
                  
              </Grid>
              <Grid item xs={3}>
                  <h3>Words</h3>
              </Grid>
              <Grid item xs={3}>
                  <h3>Words</h3>
                  
              </Grid>

        </Grid>
        </Paper>
      </div>
    );
  }
}

export default App;